#include<iostream>
#include<set>
#include<memory>

class Node{
	public:
		Node(const int& new_data):data(new_data){
			
		}
		~Node() = default;
		void set_data(const int& new_data){
			data = new_data;
		}
		int get_data() const{
			return data;
		}
	private:
		int data;
};

using shared_ptr = std::shared_ptr<Node>;

int main(int argc, char** argv){
	shared_ptr sp1 = std::make_shared<Node>(42);
	std::cout << "Smart Pointer Data -> " << sp1->get_data();
	//smart pointer dies here
	return 0;
}
